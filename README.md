# xivo-python-marshmallow-packaging

Debian packaging for [marshmallow](https://marshmallow.readthedocs.org) used in XiVO.

## Upgrading

To upgrade marshmallow:

* Update the version number in the `VERSION` file
* Update the changelog using `dch -i` to the matching version
* Push the changes
